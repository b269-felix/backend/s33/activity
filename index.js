// retrieve all data from JSON API holder
fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then((json) => console.log(json));

// returns only the title of list items 
fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then(json => {
	let titles = json.map(item => item.title);
	console.log(titles);
	}
);
// retrieve a single to do list item
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then(response => response.json())
.then((json) => console.log(json));

// print the status and title of the specific list item
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then(response => response.json())
.then(json => {
	const {title, completed} = json;
	const status = completed ? "true" : "false";
	console.log(`The item "${title}" on the list has a status of ${status}`)
	}
);

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		completed: false,
		title: "Created to do list item",
		userId: 1

	})
})
.then((response) => response.json())
.then((json) => console.log(json));


fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		dateCompleted: "Pending",
		description: "To update my to do list item with a different data structure",
		status: "Pending",
		title: "Updated to do list item",


	})
})
.then((response) => response.json())
.then((json) => console.log(json));


fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		completed: false,
		dateCompleted: "07/09/21",
		status: "Complete",
		title: "delectus aut autem"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'DELETE'
});












